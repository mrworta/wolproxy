# TL;DR
Build and run:
docker run --restart=unless-stopped --net=host -d $(docker build --tag wolproxy:latest . -q)

Usage: 
curl http://\<host-ip\>:8080/\<mac-to-wake\>
