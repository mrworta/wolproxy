import http.server
import re
from wakeonlan import send_magic_packet

PORT = 8080

""" Simple Wake on LAN Proxy to send WoL requests from other L2 Networks """

class WOL_RequestHandler(http.server.BaseHTTPRequestHandler):
    """ Handle GET request only """
    def do_GET(self):
        # simple filter for mac:
        mac=re.sub("[^a-fA-F0-9]", "", self.path)
        if (len(mac) == 12): 
            print("Try waking %s..." % mac)
            send_magic_packet(mac)
            self.send_response(200)
        else:
            print("shu. go away.")
            self.send_response(404)

        self.send_header('Content-type','text/html')
        self.end_headers()
        return


def run(server_class=http.server.HTTPServer, handler_class=WOL_RequestHandler):
    
    server_address = ('0.0.0.0', PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

run()